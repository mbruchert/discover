# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Chetan Khona <chetan@kompkin.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-04 01:00+0000\n"
"PO-Revision-Date: 2013-04-01 10:16+0530\n"
"Last-Translator: Chetan Khona <chetan@kompkin.com>\n"
"Language-Team: Marathi <kde-i18n-doc@kde.org>\n"
"Language: mr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 1.5\n"

#: discover/DiscoverObject.cpp:159
#, kde-format
msgid ""
"Discover currently cannot be used to install any apps or perform system "
"updates because none of its app backends are available."
msgstr ""

#: discover/DiscoverObject.cpp:163
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You can install some on the Settings page, under the <interface>Missing "
"Backends</interface> section.<nl/><nl/>Also please consider reporting this "
"as a packaging issue to your distribution."
msgstr ""

#: discover/DiscoverObject.cpp:168 discover/DiscoverObject.cpp:380
#: discover/qml/UpdatesPage.qml:107
#, kde-format
msgid "Report This Issue"
msgstr ""

#: discover/DiscoverObject.cpp:173
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You can use <command>pacman</command> to install the optional dependencies "
"that are needed to enable the application backends.<nl/><nl/>Please note "
"that Arch Linux developers recommend using <command>pacman</command> for "
"managing software because the PackageKit backend is not well-integrated on "
"Arch Linux."
msgstr ""

#: discover/DiscoverObject.cpp:181
#, kde-format
msgid "Learn More"
msgstr ""

#: discover/DiscoverObject.cpp:269
#, kde-format
msgid "Could not find category '%1'"
msgstr ""

#: discover/DiscoverObject.cpp:284
#, kde-format
msgid "Trying to open inexisting file '%1'"
msgstr ""

#: discover/DiscoverObject.cpp:306
#, kde-format
msgid ""
"Cannot interact with flatpak resources without the flatpak backend %1. "
"Please install it first."
msgstr ""

#: discover/DiscoverObject.cpp:310
#, kde-format
msgid "Could not open %1"
msgstr ""

#: discover/DiscoverObject.cpp:372
#, kde-format
msgid "Please make sure Snap support is installed"
msgstr ""

#: discover/DiscoverObject.cpp:374
#, kde-format
msgid ""
"Could not open %1 because it was not found in any available software "
"repositories."
msgstr ""

#: discover/DiscoverObject.cpp:377
#, kde-format
msgid "Please report this issue to the packagers of your distribution."
msgstr ""

#: discover/DiscoverObject.cpp:442 discover/DiscoverObject.cpp:443
#: discover/main.cpp:120 discover/qml/BrowsingPage.qml:20
#, kde-format
msgid "Discover"
msgstr "शोधा"

#: discover/DiscoverObject.cpp:443
#, kde-format
msgid ""
"Discover was closed before certain tasks were done, waiting for it to finish."
msgstr ""

#: discover/main.cpp:42
#, fuzzy, kde-format
#| msgid "Directly open the specified application by its package name."
msgid "Directly open the specified application by its appstream:// URI."
msgstr "निर्देशीत अनुप्रयोग त्याच्या पॅकेज नावाने उघडा."

#: discover/main.cpp:43
#, fuzzy, kde-format
#| msgid "Open with a program that can deal with the given mimetype."
msgid "Open with a search for programs that can deal with the given mimetype."
msgstr "दिलेल्या माइम प्रकाराशी चालू शकेल अशा कार्यक्रमात उघडा."

#: discover/main.cpp:44
#, kde-format
msgid "Display a list of entries with a category."
msgstr "नोंदींची यादी विभागासहित दर्शवा."

#: discover/main.cpp:45
#, fuzzy, kde-format
#| msgid ""
#| "Open Muon Discover in a said mode. Modes correspond to the toolbar "
#| "buttons."
msgid "Open Discover in a said mode. Modes correspond to the toolbar buttons."
msgstr "म्युओन डिस्कव्हर सांगितलेल्या पद्धतीने उघडा. पध्दती साधनपट्टी बटनांशी संलग्न असतात."

#: discover/main.cpp:46
#, fuzzy, kde-format
#| msgid "List all the available modes and output them on stdout."
msgid "List all the available modes."
msgstr "उपलब्ध पद्धतीची यादी करा व त्याचे आउटपुट stdout वर द्या."

#: discover/main.cpp:47
#, kde-format
msgid "Compact Mode (auto/compact/full)."
msgstr ""

#: discover/main.cpp:48
#, kde-format
msgid "Local package file to install"
msgstr ""

#: discover/main.cpp:49
#, fuzzy, kde-format
#| msgid "List all the available modes and output them on stdout."
msgid "List all the available backends."
msgstr "उपलब्ध पद्धतीची यादी करा व त्याचे आउटपुट stdout वर द्या."

#: discover/main.cpp:50
#, fuzzy, kde-format
#| msgid "Search in '%1'..."
msgid "Search string."
msgstr "यामध्ये शोधा '%1'..."

#: discover/main.cpp:51
#, kde-format
msgid "Lists the available options for user feedback"
msgstr ""

#: discover/main.cpp:53
#, kde-format
msgid "Supports appstream: url scheme"
msgstr ""

#: discover/main.cpp:122
#, fuzzy, kde-format
#| msgid "An application discoverer"
msgid "An application explorer"
msgstr "अनुप्रयोग शोधक"

#: discover/main.cpp:124
#, kde-format
msgid "© 2010-2022 Plasma Development Team"
msgstr ""

#: discover/main.cpp:125
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "एलैक्स पोल गोन्झालेझ"

#: discover/main.cpp:126
#, kde-format
msgid "Nate Graham"
msgstr ""

#: discover/main.cpp:127
#, kde-format
msgid "Quality Assurance, Design and Usability"
msgstr ""

#: discover/main.cpp:131
#, kde-format
msgid "Dan Leinir Turthra Jensen"
msgstr ""

#: discover/main.cpp:132
#, kde-format
msgid "KNewStuff"
msgstr ""

#: discover/main.cpp:139
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#: discover/main.cpp:139
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: discover/main.cpp:152
#, fuzzy, kde-format
#| msgid "Available modes:\n"
msgid "Available backends:\n"
msgstr "उपलब्ध पद्धती :\n"

#: discover/main.cpp:208
#, kde-format
msgid "Available modes:\n"
msgstr "उपलब्ध पद्धती :\n"

#: discover/qml/AddonsView.qml:18 discover/qml/navigation.js:43
#, kde-format
msgid "Addons for %1"
msgstr ""

#: discover/qml/AddonsView.qml:50
#, kde-format
msgid "More…"
msgstr ""

#: discover/qml/AddonsView.qml:59
#, kde-format
msgid "Apply Changes"
msgstr ""

#: discover/qml/AddonsView.qml:66
#, kde-format
msgid "Reset"
msgstr ""

#: discover/qml/AddSourceDialog.qml:20
#, kde-format
msgid "Add New %1 Repository"
msgstr ""

#: discover/qml/AddSourceDialog.qml:44
#, fuzzy, kde-format
#| msgid "Add-ons"
msgid "Add"
msgstr "एडोन्स"

#: discover/qml/AddSourceDialog.qml:49 discover/qml/DiscoverWindow.qml:257
#: discover/qml/InstallApplicationButton.qml:41
#: discover/qml/ProgressView.qml:104 discover/qml/SourcesPage.qml:179
#: discover/qml/UpdatesPage.qml:253 discover/qml/WebflowDialog.qml:39
#, kde-format
msgid "Cancel"
msgstr "रद्द करा"

#: discover/qml/ApplicationDelegate.qml:140
#: discover/qml/ApplicationPage.qml:208
#, fuzzy, kde-format
#| msgid "Rating"
msgid "%1 rating"
msgid_plural "%1 ratings"
msgstr[0] "गुणवत्ताश्रेणी"
msgstr[1] "गुणवत्ताश्रेणी"

#: discover/qml/ApplicationDelegate.qml:140
#: discover/qml/ApplicationPage.qml:208
#, kde-format
msgid "No ratings yet"
msgstr ""

#: discover/qml/ApplicationPage.qml:62
#, fuzzy, kde-format
#| msgid "Sources"
msgid "Sources"
msgstr "स्रोत"

#: discover/qml/ApplicationPage.qml:73
#, kde-format
msgid "%1 - %2"
msgstr ""

#: discover/qml/ApplicationPage.qml:185
#, kde-format
msgid "Unknown author"
msgstr ""

#: discover/qml/ApplicationPage.qml:220
#, kde-format
msgid "Could not access the screenshots"
msgstr ""

#: discover/qml/ApplicationPage.qml:279
#, kde-format
msgid "Version"
msgstr ""

#: discover/qml/ApplicationPage.qml:306
#: discover/qml/ApplicationsListPage.qml:109
#, fuzzy, kde-format
#| msgid "<b>Total Size:</b> %1<br/>"
msgid "Size"
msgstr "<b>एकूण आकार :</b> %1<br/>"

#: discover/qml/ApplicationPage.qml:333
#, kde-format
msgid "Distributed by"
msgstr ""

#: discover/qml/ApplicationPage.qml:360
#, fuzzy, kde-format
#| msgid "points: %1"
msgid "License"
msgid_plural "Licenses"
msgstr[0] "गुण : %1"
msgstr[1] "गुण : %1"

#: discover/qml/ApplicationPage.qml:369
#, kde-format
msgctxt "The app does not provide any licenses"
msgid "Unknown"
msgstr ""

#: discover/qml/ApplicationPage.qml:407
#, kde-format
msgid "What does this mean?"
msgstr ""

#: discover/qml/ApplicationPage.qml:417
#, kde-format
msgid "See more…"
msgid_plural "See more…"
msgstr[0] ""
msgstr[1] ""

#: discover/qml/ApplicationPage.qml:435 discover/qml/ApplicationPage.qml:880
#, fuzzy, kde-format
#| msgid "Rating"
msgid "Content Rating"
msgstr "गुणवत्ताश्रेणी"

#: discover/qml/ApplicationPage.qml:445
#, kde-format
msgid "Age: %1+"
msgstr ""

#: discover/qml/ApplicationPage.qml:469
#, kde-format
msgctxt "@action"
msgid "See details…"
msgstr ""

#: discover/qml/ApplicationPage.qml:568
#, kde-format
msgid "Documentation"
msgstr ""

#: discover/qml/ApplicationPage.qml:569
#, kde-format
msgid "Read the project's official documentation"
msgstr ""

#: discover/qml/ApplicationPage.qml:585
#, kde-format
msgid "Website"
msgstr ""

#: discover/qml/ApplicationPage.qml:586
#, kde-format
msgid "Visit the project's website"
msgstr ""

#: discover/qml/ApplicationPage.qml:602
#, kde-format
msgid "Addons"
msgstr ""

#: discover/qml/ApplicationPage.qml:603
#, kde-format
msgid "Install or remove additional functionality"
msgstr ""

#: discover/qml/ApplicationPage.qml:622
#, kde-format
msgctxt "Exports the application's URL to an external service"
msgid "Share"
msgstr ""

#: discover/qml/ApplicationPage.qml:623
#, kde-format
msgid "Send a link for this application"
msgstr ""

#: discover/qml/ApplicationPage.qml:639
#, kde-format
msgctxt "The subject line for an email. %1 is the name of an application"
msgid "Check out the %1 app!"
msgstr ""

#: discover/qml/ApplicationPage.qml:659
#, kde-format
msgid "What's New"
msgstr ""

#: discover/qml/ApplicationPage.qml:690
#, fuzzy, kde-format
#| msgid "Rating"
msgid "Loading reviews for %1"
msgstr "गुणवत्ताश्रेणी"

#: discover/qml/ApplicationPage.qml:696
#, kde-format
msgid "Reviews"
msgstr "प्रतिक्रिया"

#: discover/qml/ApplicationPage.qml:734
#, fuzzy, kde-format
#| msgid "%1 reviews"
msgid "Show all %1 Reviews"
msgid_plural "Show all %1 Reviews"
msgstr[0] "%1 प्रतिक्रिया"
msgstr[1] "%1 प्रतिक्रिया"

#: discover/qml/ApplicationPage.qml:746
#, fuzzy, kde-format
#| msgid "Submit"
msgid "Write a Review"
msgstr "सादर करा"

#: discover/qml/ApplicationPage.qml:746
#, fuzzy, kde-format
#| msgid "Submit"
msgid "Install to Write a Review"
msgstr "सादर करा"

#: discover/qml/ApplicationPage.qml:758
#, kde-format
msgid "Get Involved"
msgstr ""

#: discover/qml/ApplicationPage.qml:791
#, kde-format
msgid "Donate"
msgstr ""

#: discover/qml/ApplicationPage.qml:792
#, kde-format
msgid "Support and thank the developers by donating to their project"
msgstr ""

#: discover/qml/ApplicationPage.qml:808
#, kde-format
msgid "Report Bug"
msgstr ""

#: discover/qml/ApplicationPage.qml:809
#, kde-format
msgid "Log an issue you found to help get it fixed"
msgstr ""

#: discover/qml/ApplicationPage.qml:823
#, kde-format
msgid "Contribute"
msgstr ""

#: discover/qml/ApplicationPage.qml:824
#, kde-format
msgid "Help the developers by coding, designing, testing, or translating"
msgstr ""

#: discover/qml/ApplicationPage.qml:849
#, fuzzy, kde-format
#| msgid "points: %1"
msgid "All Licenses"
msgstr "गुण : %1"

#: discover/qml/ApplicationPage.qml:894
#, kde-format
msgid "Risks of proprietary software"
msgstr ""

#: discover/qml/ApplicationPage.qml:900
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (<link url='%1'>%2</link>).<nl/><nl/>You can "
"learn more at <link url='%3'>%3</link>."
msgstr ""

#: discover/qml/ApplicationPage.qml:901
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (%1).<nl/><nl/>You can learn more at <link "
"url='%2'>%2</link>."
msgstr ""

#: discover/qml/ApplicationsListPage.qml:53
#, fuzzy, kde-format
#| msgid "Search in '%1'..."
msgid "Search: %2 - %3 item"
msgid_plural "Search: %2 - %3 items"
msgstr[0] "यामध्ये शोधा '%1'..."
msgstr[1] "यामध्ये शोधा '%1'..."

#: discover/qml/ApplicationsListPage.qml:55
#, fuzzy, kde-format
#| msgid "Search in '%1'..."
msgid "Search: %1"
msgstr "यामध्ये शोधा '%1'..."

#: discover/qml/ApplicationsListPage.qml:59
#, fuzzy, kde-format
#| msgid "Search in '%1'..."
msgid "%2 - %1 item"
msgid_plural "%2 - %1 items"
msgstr[0] "यामध्ये शोधा '%1'..."
msgstr[1] "यामध्ये शोधा '%1'..."

#: discover/qml/ApplicationsListPage.qml:65
#, fuzzy, kde-format
#| msgid "Search in '%1'..."
msgid "Search - %1 item"
msgid_plural "Search - %1 items"
msgstr[0] "यामध्ये शोधा '%1'..."
msgstr[1] "यामध्ये शोधा '%1'..."

#: discover/qml/ApplicationsListPage.qml:67
#: discover/qml/ApplicationsListPage.qml:225
#, fuzzy, kde-format
#| msgid "Search..."
msgid "Search"
msgstr "शोधा..."

#: discover/qml/ApplicationsListPage.qml:88
#, fuzzy, kde-format
#| msgid "Search in '%1'..."
msgid "Sort: %1"
msgstr "यामध्ये शोधा '%1'..."

#: discover/qml/ApplicationsListPage.qml:91
#, kde-format
msgid "Name"
msgstr "नाव"

#: discover/qml/ApplicationsListPage.qml:100
#, kde-format
msgid "Rating"
msgstr "गुणवत्ताश्रेणी"

#: discover/qml/ApplicationsListPage.qml:118
#, kde-format
msgid "Release Date"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:173
#, kde-format
msgid "Nothing found"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:180
#, kde-format
msgctxt "@action:button"
msgid "Search in All Categories"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:189
#, fuzzy, kde-format
#| msgid "Specify the new source"
msgctxt "@action:button %1 is the name of an application"
msgid "Search the Web for \"%1\""
msgstr "नवीन स्रोत निर्देशीत करा"

#: discover/qml/ApplicationsListPage.qml:193
#, kde-format
msgctxt ""
"If appropriate, localize this URL to be something more relevant to the "
"language. %1 is the text that will be searched for."
msgid "https://duckduckgo.com/?q=%1"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:204
#, kde-format
msgctxt ""
"@info:placeholder %1 is the name of an application; %2 is the name of a "
"category of apps or add-ons"
msgid "\"%1\" was not found in the \"%2\" category"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:206
#, kde-format
msgctxt "@info:placeholder %1 is the name of an application"
msgid "\"%1\" was not found in the available sources"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:207
#, kde-format
msgctxt "@info:placeholder%1 is the name of an application"
msgid ""
"\"%1\" may be available on the web. Software acquired from the web has not "
"been reviewed by your distributor for functionality or stability. Use with "
"caution."
msgstr ""

#: discover/qml/ApplicationsListPage.qml:240
#, fuzzy, kde-format
#| msgid "Install"
msgid "Still looking…"
msgstr "प्रतिष्ठापीत करा"

#: discover/qml/BrowsingPage.qml:49
#, kde-format
msgid "Unable to load applications"
msgstr ""

#: discover/qml/BrowsingPage.qml:88
#, fuzzy, kde-format
#| msgid "Popularity"
msgctxt "@title:group"
msgid "Most Popular"
msgstr "लोकप्रियता"

#: discover/qml/BrowsingPage.qml:106
#, kde-format
msgctxt "@title:group"
msgid "Editor's Choice"
msgstr ""

#: discover/qml/BrowsingPage.qml:120
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Games"
msgstr ""

#: discover/qml/BrowsingPage.qml:139 discover/qml/BrowsingPage.qml:168
#, kde-format
msgctxt "@action:button"
msgid "See More"
msgstr ""

#: discover/qml/BrowsingPage.qml:149
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Developer Tools"
msgstr ""

#: discover/qml/DiscoverWindow.qml:43
#, kde-format
msgid "Running as <em>root</em> is discouraged and unnecessary."
msgstr ""

#: discover/qml/DiscoverWindow.qml:56
#, fuzzy, kde-format
#| msgid "Homepage"
msgid "&Home"
msgstr "मुख्यपान"

#: discover/qml/DiscoverWindow.qml:66
#, fuzzy, kde-format
#| msgid "Search..."
msgid "&Search"
msgstr "शोधा..."

#: discover/qml/DiscoverWindow.qml:74
#, fuzzy, kde-format
#| msgid "Installed"
msgid "&Installed"
msgstr "प्रतिष्ठापीत केलेले"

#: discover/qml/DiscoverWindow.qml:81
#, fuzzy, kde-format
#| msgid "Update"
msgid "Fetching &updates…"
msgstr "अद्ययावत करा"

#: discover/qml/DiscoverWindow.qml:81
#, fuzzy, kde-format
#| msgid "Update"
msgid "&Up to date"
msgstr "अद्ययावत करा"

#: discover/qml/DiscoverWindow.qml:81
#, fuzzy, kde-format
#| msgid "Update"
msgctxt "Update section name"
msgid "&Update (%1)"
msgstr "अद्ययावत करा"

#: discover/qml/DiscoverWindow.qml:88
#, kde-format
msgid "&About"
msgstr ""

#: discover/qml/DiscoverWindow.qml:96
#, fuzzy, kde-format
#| msgid "Best Ratings"
msgid "S&ettings"
msgstr "सर्वोत्कृष्ट गुणवत्ताश्रेणी"

#: discover/qml/DiscoverWindow.qml:146 discover/qml/DiscoverWindow.qml:325
#: discover/qml/DiscoverWindow.qml:432
#, kde-format
msgid "Error"
msgstr ""

#: discover/qml/DiscoverWindow.qml:150
#, kde-format
msgid "Unable to find resource: %1"
msgstr ""

#: discover/qml/DiscoverWindow.qml:244 discover/qml/SourcesPage.qml:169
#, kde-format
msgid "Proceed"
msgstr ""

#: discover/qml/DiscoverWindow.qml:301
#, kde-format
msgid "Report this issue"
msgstr ""

#: discover/qml/DiscoverWindow.qml:325
#, kde-format
msgid "Error %1 of %2"
msgstr ""

#: discover/qml/DiscoverWindow.qml:370
#, kde-format
msgctxt "@action:button"
msgid "Show Previous"
msgstr ""

#: discover/qml/DiscoverWindow.qml:383
#, kde-format
msgctxt "@action:button"
msgid "Show Next"
msgstr ""

#: discover/qml/DiscoverWindow.qml:399 discover/qml/UpdatesPage.qml:98
#, kde-format
msgid "Copy to Clipboard"
msgstr ""

#: discover/qml/Feedback.qml:12
#, kde-format
msgid "Submit usage information"
msgstr ""

#: discover/qml/Feedback.qml:13
#, kde-format
msgid ""
"Sends anonymized usage information to KDE so we can better understand our "
"users. For more information see https://kde.org/privacypolicy-apps.php."
msgstr ""

#: discover/qml/Feedback.qml:17
#, kde-format
msgid "Submitting usage information…"
msgstr ""

#: discover/qml/Feedback.qml:17
#, kde-format
msgid "Configure"
msgstr ""

#: discover/qml/Feedback.qml:21
#, kde-format
msgid "Configure feedback…"
msgstr ""

#: discover/qml/Feedback.qml:28 discover/qml/SourcesPage.qml:19
#, fuzzy, kde-format
#| msgid "Update"
msgid "Configure Updates…"
msgstr "अद्ययावत करा"

#: discover/qml/Feedback.qml:58
#, kde-format
msgid ""
"You can help us improving this application by sharing statistics and "
"participate in surveys."
msgstr ""

#: discover/qml/Feedback.qml:58
#, kde-format
msgid "Contribute…"
msgstr ""

#: discover/qml/Feedback.qml:63
#, kde-format
msgid "We are looking for your feedback!"
msgstr ""

#: discover/qml/Feedback.qml:63
#, kde-format
msgid "Participate…"
msgstr ""

#: discover/qml/InstallApplicationButton.qml:25
#, fuzzy, kde-format
#| msgid "Loading..."
msgctxt "State being fetched"
msgid "Loading…"
msgstr "दाखल करत आहे..."

#: discover/qml/InstallApplicationButton.qml:28
#, kde-format
msgid "Install"
msgstr "प्रतिष्ठापीत करा"

#: discover/qml/InstallApplicationButton.qml:30
#, kde-format
msgid "Remove"
msgstr "काढून टाका"

#: discover/qml/InstalledPage.qml:15
#, kde-format
msgid "Installed"
msgstr "प्रतिष्ठापीत केलेले"

#: discover/qml/navigation.js:19
#, kde-format
msgid "Resources for '%1'"
msgstr ""

#: discover/qml/ProgressView.qml:17
#, fuzzy, kde-format
#| msgid "Update"
msgid "Tasks (%1%)"
msgstr "अद्ययावत करा"

#: discover/qml/ProgressView.qml:17 discover/qml/ProgressView.qml:40
#, kde-format
msgid "Tasks"
msgstr ""

#: discover/qml/ProgressView.qml:97
#, kde-format
msgctxt "TransactioName - TransactionStatus: speed, remaining time"
msgid "%1 - %2: %3, %4 remaining"
msgstr ""

#: discover/qml/ProgressView.qml:98
#, kde-format
msgctxt "TransactioName - TransactionStatus: speed"
msgid "%1 - %2: %3"
msgstr ""

#: discover/qml/ProgressView.qml:99
#, kde-format
msgctxt "TransactioName - TransactionStatus"
msgid "%1 - %2"
msgstr ""

#: discover/qml/ReviewDelegate.qml:61
#, kde-format
msgid "unknown reviewer"
msgstr ""

#: discover/qml/ReviewDelegate.qml:62
#, fuzzy, kde-format
#| msgid "<b>%1</b><br/>%2"
msgid "<b>%1</b> by %2"
msgstr "<b>%1</b><br/>%2"

#: discover/qml/ReviewDelegate.qml:62
#, kde-format
msgid "Comment by %1"
msgstr ""

#: discover/qml/ReviewDelegate.qml:80
#, kde-format
msgid "Version: %1"
msgstr ""

#: discover/qml/ReviewDelegate.qml:80
#, kde-format
msgid "Version: unknown"
msgstr ""

#: discover/qml/ReviewDelegate.qml:95
#, kde-format
msgid "Votes: %1 out of %2"
msgstr ""

#: discover/qml/ReviewDelegate.qml:102
#, kde-format
msgid "Was this review useful?"
msgstr ""

#: discover/qml/ReviewDelegate.qml:114
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "Yes"
msgstr ""

#: discover/qml/ReviewDelegate.qml:131
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "No"
msgstr ""

#: discover/qml/ReviewDialog.qml:18
#, fuzzy, kde-format
#| msgid "Reviewing %1"
msgid "Reviewing %1"
msgstr "समीक्षा करत आहे %1"

#: discover/qml/ReviewDialog.qml:24
#, fuzzy, kde-format
#| msgid "Submit"
msgid "Submit review"
msgstr "सादर करा"

#: discover/qml/ReviewDialog.qml:37
#, kde-format
msgid "Rating:"
msgstr "गुणवत्ताश्रेणी :"

#: discover/qml/ReviewDialog.qml:42
#, fuzzy, kde-format
#| msgid "Name"
msgid "Name:"
msgstr "नाव"

#: discover/qml/ReviewDialog.qml:50
#, kde-format
msgid "Title:"
msgstr ""

#: discover/qml/ReviewDialog.qml:67
#, kde-format
msgid "Enter a rating"
msgstr ""

#: discover/qml/ReviewDialog.qml:68
#, fuzzy, kde-format
#| msgid "Submit"
msgid "Write the title"
msgstr "सादर करा"

#: discover/qml/ReviewDialog.qml:69
#, fuzzy, kde-format
#| msgid "Submit"
msgid "Write the review"
msgstr "सादर करा"

#: discover/qml/ReviewDialog.qml:70
#, fuzzy, kde-format
#| msgid "Loading..."
msgid "Keep writing…"
msgstr "दाखल करत आहे..."

#: discover/qml/ReviewDialog.qml:71
#, kde-format
msgid "Too long!"
msgstr ""

#: discover/qml/ReviewDialog.qml:72
#, kde-format
msgctxt "@info:usagetip"
msgid "Insert a name"
msgstr ""

#: discover/qml/ReviewsPage.qml:42
#, fuzzy, kde-format
#| msgid "Rating"
msgid "Reviews for %1"
msgstr "गुणवत्ताश्रेणी"

#: discover/qml/ReviewsPage.qml:53
#, fuzzy, kde-format
#| msgid "Submit"
msgid "Write a Review…"
msgstr "सादर करा"

#: discover/qml/ReviewsPage.qml:58
#, kde-format
msgid "Install this app to write a review"
msgstr ""

#: discover/qml/SearchField.qml:24
#, fuzzy, kde-format
#| msgid "Search..."
msgid "Search…"
msgstr "शोधा..."

#: discover/qml/SearchField.qml:24
#, fuzzy, kde-format
#| msgid "Search in '%1'..."
msgid "Search in '%1'…"
msgstr "यामध्ये शोधा '%1'..."

#: discover/qml/SourcesPage.qml:13
#, fuzzy, kde-format
#| msgid "Best Ratings"
msgid "Settings"
msgstr "सर्वोत्कृष्ट गुणवत्ताश्रेणी"

#: discover/qml/SourcesPage.qml:93
#, fuzzy, kde-format
#| msgid "Sources"
msgid "Default source"
msgstr "स्रोत"

#: discover/qml/SourcesPage.qml:100
#, fuzzy, kde-format
#| msgid "Add Source"
msgid "Add Source…"
msgstr "स्रोत जोडा"

#: discover/qml/SourcesPage.qml:124
#, kde-format
msgid "Make default"
msgstr ""

#: discover/qml/SourcesPage.qml:210
#, kde-format
msgid "Increase priority"
msgstr ""

#: discover/qml/SourcesPage.qml:216
#, kde-format
msgid "Failed to increase '%1' preference"
msgstr ""

#: discover/qml/SourcesPage.qml:221
#, kde-format
msgid "Decrease priority"
msgstr ""

#: discover/qml/SourcesPage.qml:227
#, kde-format
msgid "Failed to decrease '%1' preference"
msgstr ""

#: discover/qml/SourcesPage.qml:232
#, kde-format
msgid "Remove repository"
msgstr ""

#: discover/qml/SourcesPage.qml:243
#, kde-format
msgid "Show contents"
msgstr ""

#: discover/qml/SourcesPage.qml:282
#, kde-format
msgid "Missing Backends"
msgstr ""

#: discover/qml/UpdatesPage.qml:12
#, fuzzy, kde-format
#| msgid "Update"
msgid "Updates"
msgstr "अद्ययावत करा"

#: discover/qml/UpdatesPage.qml:45
#, fuzzy, kde-format
#| msgid "Update"
msgid "Update Issue"
msgstr "अद्ययावत करा"

#: discover/qml/UpdatesPage.qml:45
#, kde-format
msgid "Technical details"
msgstr ""

#: discover/qml/UpdatesPage.qml:61
#, kde-format
msgid "There was an issue installing this update. Please try again later."
msgstr ""

#: discover/qml/UpdatesPage.qml:67
#, kde-format
msgid "See Technical Details"
msgstr ""

#: discover/qml/UpdatesPage.qml:83
#, kde-format
msgid ""
"If you would like to report the update issue to your distribution's "
"packagers, include this information:"
msgstr ""

#: discover/qml/UpdatesPage.qml:102
#, kde-format
msgid "Error message copied to clipboard"
msgstr ""

#: discover/qml/UpdatesPage.qml:134
#, fuzzy, kde-format
#| msgid "Update All"
msgid "Update Selected"
msgstr "सर्व अद्ययावत करा"

#: discover/qml/UpdatesPage.qml:134
#, kde-format
msgid "Update All"
msgstr "सर्व अद्ययावत करा"

#: discover/qml/UpdatesPage.qml:174
#, kde-format
msgid "Ignore"
msgstr ""

#: discover/qml/UpdatesPage.qml:220
#, kde-format
msgid "Select All"
msgstr ""

#: discover/qml/UpdatesPage.qml:228
#, kde-format
msgid "Select None"
msgstr ""

#: discover/qml/UpdatesPage.qml:235
#, kde-format
msgid "Restart automatically after update has completed"
msgstr ""

#: discover/qml/UpdatesPage.qml:242
#, kde-format
msgid "Total size: %1"
msgstr ""

#: discover/qml/UpdatesPage.qml:278
#, kde-format
msgid "Restart Now"
msgstr ""

#: discover/qml/UpdatesPage.qml:377
#, kde-format
msgid "%1"
msgstr ""

#: discover/qml/UpdatesPage.qml:393
#, fuzzy, kde-format
#| msgid "Install"
msgid "Installing"
msgstr "प्रतिष्ठापीत करा"

#: discover/qml/UpdatesPage.qml:428
#, fuzzy, kde-format
#| msgid "Update All"
msgid "Update from:"
msgstr "सर्व अद्ययावत करा"

#: discover/qml/UpdatesPage.qml:440
#, kde-format
msgctxt ""
"%1 is the backend that provides this app, %2 is the specific repository or "
"address within that backend"
msgid "%1 (%2)"
msgstr ""

#: discover/qml/UpdatesPage.qml:447
#, kde-format
msgid "More Information…"
msgstr ""

#: discover/qml/UpdatesPage.qml:475
#, fuzzy, kde-format
#| msgid "Update"
msgctxt "@info"
msgid "Fetching updates…"
msgstr "अद्ययावत करा"

#: discover/qml/UpdatesPage.qml:488
#, fuzzy, kde-format
#| msgid "Update"
msgctxt "@info"
msgid "Updates"
msgstr "अद्ययावत करा"

#: discover/qml/UpdatesPage.qml:498
#, kde-format
msgctxt "@info"
msgid "Restart the system to complete the update process"
msgstr ""

#: discover/qml/UpdatesPage.qml:510 discover/qml/UpdatesPage.qml:517
#: discover/qml/UpdatesPage.qml:524
#, fuzzy, kde-format
#| msgid "Update"
msgctxt "@info"
msgid "Up to date"
msgstr "अद्ययावत करा"

#: discover/qml/UpdatesPage.qml:531
#, kde-format
msgctxt "@info"
msgid "Should check for updates"
msgstr ""

#: discover/qml/UpdatesPage.qml:538
#, kde-format
msgctxt "@info"
msgid "Time of last update unknown"
msgstr ""

#, fuzzy
#~| msgid "Loading..."
#~ msgid "Loading…"
#~ msgstr "दाखल करत आहे..."

#, fuzzy
#~| msgid "<b>Total Size:</b> %1<br/>"
#~ msgid "Size:"
#~ msgstr "<b>एकूण आकार :</b> %1<br/>"

#, fuzzy
#~| msgid "Sources"
#~ msgid "Source:"
#~ msgstr "स्रोत"

#, fuzzy
#~| msgid "Update All"
#~ msgid "All updates selected (%1)"
#~ msgstr "सर्व अद्ययावत करा"

#, fuzzy
#~| msgid "Update All"
#~ msgid "%1/%2 update selected (%3)"
#~ msgid_plural "%1/%2 updates selected (%3)"
#~ msgstr[0] "सर्व अद्ययावत करा"
#~ msgstr[1] "सर्व अद्ययावत करा"

#~ msgid "OK"
#~ msgstr "ठीक आहे"

#, fuzzy
#~| msgid "Install"
#~ msgctxt "Install the version of an app that comes from Snap, Flatpak, etc"
#~ msgid "Install from %1"
#~ msgstr "प्रतिष्ठापीत करा"

#, fuzzy
#~| msgid "Loading..."
#~ msgctxt "State being fetched"
#~ msgid "Loading..."
#~ msgstr "दाखल करत आहे..."

#, fuzzy
#~| msgid "Submit"
#~ msgid "Write a Review..."
#~ msgstr "सादर करा"

#~ msgid "Search..."
#~ msgstr "शोधा..."

#, fuzzy
#~| msgid "Submit"
#~ msgid "Write a review!"
#~ msgstr "सादर करा"

#~ msgid "Jonathan Thomas"
#~ msgstr "जोनाथन थोमस"

#~ msgid "Discard"
#~ msgstr "सोडून द्या"

#, fuzzy
#~| msgid "Homepage"
#~ msgid "Homepage:"
#~ msgstr "मुख्यपान"

#, fuzzy
#~| msgid "Update"
#~ msgid "Fetching Updates..."
#~ msgstr "अद्ययावत करा"

#, fuzzy
#~| msgid "Update"
#~ msgctxt "@info"
#~ msgid "Fetching Updates..."
#~ msgstr "अद्ययावत करा"

#~ msgid "<em>Tell us about this review!</em>"
#~ msgstr "<em>या प्रतिक्रियेबद्दल आम्हाला सांगा !</em>"

#~ msgid "<em>%1 out of %2 people found this review useful</em>"
#~ msgstr "<em>%2 पैकी %1 व्यक्तिंना हि प्रतिक्रिया उपयोगी वाटली</em>"

#~ msgid ""
#~ "<em>Useful? <a href='true'><b>Yes</b></a>/<a href='false'>No</a></em>"
#~ msgstr ""
#~ "<em>उपयोगी आहे का? <a href='true'><b>होय</b></a>/<a href='false'>नाही</a></"
#~ "em>"

#~ msgid ""
#~ "<em>Useful? <a href='true'>Yes</a>/<a href='false'><b>No</b></a></em>"
#~ msgstr ""
#~ "<em>उपयोगी आहे का? <a href='true'>होय</a>/<a href='false'><b>नाही</b></a></"
#~ "em>"

#~ msgid "<em>Useful? <a href='true'>Yes</a>/<a href='false'>No</a></em>"
#~ msgstr ""
#~ "<em>उपयोगी आहे का? <a href='true'>होय</a>/<a href='false'>नाही</a></em>"

#, fuzzy
#~| msgid "Review"
#~ msgid "Review:"
#~ msgstr "समीक्षा"

#, fuzzy
#~| msgid "Review"
#~ msgid "Review..."
#~ msgstr "समीक्षा"

#, fuzzy
#~| msgid "Update"
#~ msgid "Checking for updates..."
#~ msgstr "अद्ययावत करा"

#, fuzzy
#~| msgid "Update"
#~ msgid "No Updates"
#~ msgstr "अद्ययावत करा"

#, fuzzy
#~| msgid "Loading..."
#~ msgctxt "@info"
#~ msgid "Fetching..."
#~ msgstr "दाखल करत आहे..."

#, fuzzy
#~| msgid "Update"
#~ msgctxt "@info"
#~ msgid "Checking for updates..."
#~ msgstr "अद्ययावत करा"

#, fuzzy
#~| msgid "Loading..."
#~ msgctxt "@info"
#~ msgid "Updating..."
#~ msgstr "दाखल करत आहे..."

#, fuzzy
#~| msgid "Update"
#~ msgctxt "@info"
#~ msgid "No updates"
#~ msgstr "अद्ययावत करा"

#, fuzzy
#~| msgid "Update"
#~ msgctxt "@info"
#~ msgid "Looking for updates"
#~ msgstr "अद्ययावत करा"

#~ msgid "Summary:"
#~ msgstr "सारांश :"

#~ msgid "Back"
#~ msgstr "मागे"

#~ msgid "Launch"
#~ msgstr "प्रक्षेपण करा"

#, fuzzy
#~| msgid "An application discoverer"
#~ msgid "Application Sources"
#~ msgstr "अनुप्रयोग शोधक"

#, fuzzy
#~| msgid "Loading..."
#~ msgctxt "@info"
#~ msgid "Loading..."
#~ msgstr "दाखल करत आहे..."

#~ msgid ""
#~ "<p style='margin: 0 0 0 0'><b>%1</b> by %2</p><p style='margin: 0 0 0 0'>"
#~ "%3</p><p style='margin: 0 0 0 0'>%4</p>"
#~ msgstr ""
#~ "<p style='margin: 0 0 0 0'><b>%1</b> by %2</p><p style='margin: 0 0 0 0'>"
#~ "%3</p><p style='margin: 0 0 0 0'>%4</p>"

#~ msgid "Popularity"
#~ msgstr "लोकप्रियता"

#~ msgid "Buzz"
#~ msgstr "बोलले जाणारे"

#~ msgid "Origin"
#~ msgstr "मूळ"

#~ msgid "List"
#~ msgstr "यादी"

#~ msgid "Grid"
#~ msgstr "जाळे"

#, fuzzy
#~| msgid "Installed"
#~ msgid "items installed"
#~ msgstr "प्रतिष्ठापीत केलेले"

#, fuzzy
#~| msgid "Update"
#~ msgid "System Update"
#~ msgstr "अद्ययावत करा"

#~ msgid ""
#~ "Found some errors while setting up the GUI, the application can't proceed."
#~ msgstr "GUI संयोजीत करताना काही त्रुटी आढळल्यामुळे अनुप्रयोग पुढे जाऊ शकत नाही."

#~ msgid "Initialization error"
#~ msgstr "प्रारंभ करताना त्रुटी"

#~ msgid "Muon Discover"
#~ msgstr "म्युओन डिस्कव्हर"

#~ msgid ""
#~ "Welcome to\n"
#~ "Muon Discover!"
#~ msgstr ""
#~ "म्युओन डिस्कव्हर\n"
#~ "मध्ये तुमचे स्वागत आहे!"

#~ msgid "Configure and learn about Muon Discover"
#~ msgstr "म्युओन डिस्कव्हर विषयी शिका व संयोजीत करा"

#~ msgid "Installed (%1 update)"
#~ msgid_plural "Installed (%1 updates)"
#~ msgstr[0] "प्रतिष्ठापीत केलेले (%1 अद्ययावत)"
#~ msgstr[1] "प्रतिष्ठापीत केलेले (%1 अद्ययावत)"

#~ msgid "Menu"
#~ msgstr "मेन्यू"

#, fuzzy
#~| msgid "Origin"
#~ msgid "Origin enabled"
#~ msgstr "मूळ"

#~ msgid "©2010-2012 Jonathan Thomas"
#~ msgstr "©2010-2012 जोनाथन थोमस"

#~ msgid ""
#~ "List all the backends we'll want to have loaded, separated by coma ','."
#~ msgstr "जे बॅकएन्ड दाखल होणे गरजेचे आहेत त्यांची स्वल्पविरामाने ',' विभाजीत यादी."

#~ msgid "Close"
#~ msgstr "बंद करा"

#~ msgid ""
#~ "<sourceline> - The apt repository source line to add. This is one of:\n"
#~ msgstr ""
#~ "<sourceline> - जोडण्याकरिता एप्ट रीपोझिटरी स्रोत ओळ. हि पुढील पैकी एक :\n"

#~ msgid "%1 (Binary)"
#~ msgstr "%1 (बायनरी)"

#~ msgid "%1. %2"
#~ msgstr "%1. %2"

#~ msgid "Popularity Contest"
#~ msgstr "लोकप्रियता प्रतियोगिता"

#~ msgid "<qt>%1<br/><em>%2</em></qt>"
#~ msgstr "<qt>%1<br/><em>%2</em></qt>"

#~ msgid "Apply"
#~ msgstr "लागू करा"

#~ msgid "Overview"
#~ msgstr "पूर्वावलोकन"

#~ msgid "<b>Reviews:</b>"
#~ msgstr "<b>प्रतिक्रिया :</b>"

#~ msgid "Login"
#~ msgstr "प्रवेश"

#~ msgid "<b>%1</b> by %2<p/>%3<br/>%4"
#~ msgstr "<b>%1</b> प्रमाणे  %2<p/>%3<br/>%4"

#~ msgid "Ok"
#~ msgstr "ठीक आहे"
